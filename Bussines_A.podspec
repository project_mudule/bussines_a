Pod::Spec.new do |spec|
  # 库的名称
  spec.name         = "Bussines_A"
  # 库的版本
  spec.version      = "1.1.9"
  # 库的简单描述
  spec.summary      = "业务模块A"
  # 库的详细描述
  spec.description  = "业务模块A"
  # 库的主页               
  spec.homepage     = "https://gitlab.com/project_mudule/bussines_a"
  # 开放协议根据自己的需要填写
  spec.license      = "MIT"
  # 作者
  spec.author             = { "程旭东" => "417296350@qq.com" }
  # 支持的平台和最低系统
  spec.platform     = :ios, "9.0"
  # 代码在git上仓库的地址
  spec.source       = { :git => "https://gitlab.com/project_mudule/bussines_a.git", :tag => spec.version }
  # 声明库的源代码的位置，库的真正路径（一般是相对路径）所以这个地方不能填错。这个目录下的文件都会进行编译
  spec.source_files  = "Bussines_A/Bussines_A/Bsn_A_Config.h"
  spec.subspec 'VIewController' do |ss|
    ss.source_files = 'Bussines_A/Bussines_A/VIewController/*.{h,m}'
  end
  spec.subspec 'Dog' do |ss|
    ss.source_files = 'Bussines_A/Bussines_A/Dog/*.{h,m}'
  end
  spec.subspec 'BsnExtend' do |ss|
    #对ource_files设置要从文件的根路径理解，根路径就是当前podspec文件所在的级就是根，然后写路径即可。
    ss.source_files = 'Bussines_A/Bussines_A/BsnExtend/*.{h,m}'
    #对dependency理解不能从根路径理解，而是应该从类似于Podfile文件引入三方库的思路，即便是自身也是从三方库的角度引入，
    #三方库也是可以路径形式逐步引入的，如:ss.dependency AFNNetWork 也可以引入 ss.dependency AFNNetWork/NSURLConnect
    ss.dependency 'Bussines_A/VIewController'
  end
  # 存放我们不想参与编译的资源文件
  spec.resource  = "*.jpg", "*.md", "*.mobileprovision"
  # 存放我们不想参与编译的资源文件
     #spec.framework  = "SomeFramework"
  # 存放我们开发库中依赖的三方库
  spec.dependency 'BaseLib'
  spec.dependency 'MBProgressHUD'
end

