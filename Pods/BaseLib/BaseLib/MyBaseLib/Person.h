//
//  Person.h
//  BaseLib
//
//  Created by 程旭东 on 2021/6/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject
@property (nonatomic,strong) NSString *name;
- (void)fly;
@end

NS_ASSUME_NONNULL_END
