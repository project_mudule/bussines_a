//
//  BsnA_ViewController.h
//  Bussines_A
//
//  Created by 程旭东 on 2021/6/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BsnA_ViewController : UIViewController
- (void)eat:(NSString *)food;
@end

NS_ASSUME_NONNULL_END
