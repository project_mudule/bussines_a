//
//  Target_BsnA_ViewController.h
//  Bussines_A
//
//  Created by 程旭东 on 2021/6/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Target_BsnA_ViewController : NSObject
- (UIViewController *)Action_BsnA_ViewController:(NSDictionary *)pramas;
- (void)Action_BsnA_ViewController_eat:(NSDictionary *)food;
@end

NS_ASSUME_NONNULL_END
